# Ejemplo de Aplicación Jakarta EE

Este un ejemplo de implementación de Jakarte EE 8 que incluye (hasta el momento):

* JSF
* EJB
* JPA

## Ejecución en un contenedor 

### con Payara + MySQL

Primero hay que construir y preparar las bibliotecas y archivos de despliegue:

```
mvn clean install -P payara
```

Luego, ejecutamos el contenedor con `docker-compose` :

```
 docker-compose -f containers/payara/docker-compose.yml up
```